#!/bin/bash
set -eu

readonly config_file=/app/data/config.yml

if [[ ! -f /app/data/.hmac_key ]]; then
    echo $(openssl rand -base64 48) > /app/data/.hmac_key

    # create user at the first run
    ADMIN_PASSWORD_HASH=$(python3 -c 'import bcrypt; print(bcrypt.hashpw(b"changeme", bcrypt.gensalt(rounds=10)).decode("ascii"))')
fi 
readonly hmac_key=$(cat /app/data/.hmac_key)

generate_trusted_session() {
    echo "==> Generating trusted session"
    cd /app/code/youtube-trusted-session-generator/

    echo "  ==> internally launching GUI (X11 environment)"
    XVFB_WHD=${XVFB_WHD:-1280x720x16}

    echo "  ==> starting Xvfb"
    Xvfb :99 -ac -screen 0 $XVFB_WHD -nolisten tcp &
    pid=$!
    sleep 5

    echo "  ==> launching chromium instance"

    # Run python script on display 0
    DISPLAY=:99 python3 potoken-generator.py --oneshot | grep -E '(visitor_data|po_token)' > /app/data/.youtube-trusted-session-generator

    echo "  ==> Stopping Xvfb"
    kill -SIGTERM ${pid} > /dev/null 2>&1 | true
    cd /app/code
}

# Generate trusted session
#if [[ ! -f /app/data/.youtube-trusted-session-generator ]]; then
    generate_trusted_session
#fi
visitor_data=$(cat /app/data/.youtube-trusted-session-generator | grep -E '(visitor_data|po_token)' | yq ".visitor_data")
po_token=$(cat /app/data/.youtube-trusted-session-generator | grep -E '(visitor_data|po_token)' | yq ".po_token")

if [[ -z ${visitor_data:-} ]] || [[ -z ${po_token:-} ]]; then
    echo "Error: No trusted session generated"
    echo "Try to remove /app/data/.youtube-trusted-session-generator and restart the app"
#    echo "If it fails further you should ssh to Cloudron host and obtain trusted session values `visitor_data` and `po_token` by running:"
#    echo "  $ docker run quay.io/invidious/youtube-trusted-session-generator"
#    echo "Then copy the obtained `visitor_data` and `po_token` to /app/data/.youtube-trusted-session-generator file and restart the app"
    exit 1
fi

[[ ! -f ${config_file} ]] && cp /app/code/config/config.example.yml ${config_file}

echo "==> Update configuration"
cat $config_file |
    yq ".db.user = \"${CLOUDRON_POSTGRESQL_USERNAME}\"" | \
    yq ".db.password = \"${CLOUDRON_POSTGRESQL_PASSWORD}\"" | \
    yq ".db.host = \"${CLOUDRON_POSTGRESQL_HOST}\"" | \
    yq ".db.port = ${CLOUDRON_POSTGRESQL_PORT}" | \
    yq ".db.dbname = \"${CLOUDRON_POSTGRESQL_DATABASE}\"" | \
    yq ".domain = \"${CLOUDRON_APP_DOMAIN}\"" | \
    yq ".https_only = true" | \
    yq ".hmac_key = \"${hmac_key}\"" | \
    yq ".admins = [\"admin\"]" | \
    sed -E -e 's/#(external_port|port)*:/\1:/' | \
    yq ".port = 3000" | \
    yq ".external_port = 443" | \
    yq ".signature_server = \"127.0.0.1:12999\"" | \
    yq ".visitor_data = \"${visitor_data}\"" | \
    yq ".po_token = \"${po_token}\"" | \
    yq ".http_proxy = null" | \
    sponge $config_file

echo "==> Ensure permissions"
chown -R cloudron:cloudron /app/data

echo "==> Update DB"
gosu cloudron:cloudron /app/code/invidious --migrate

# create user at the first run
if [[ -n ${ADMIN_PASSWORD_HASH:-} ]]; then
    ADMIN_EXISTS=$(PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -AXqtc "SELECT count(*) FROM users u WHERE email='admin'");
    if [ ${ADMIN_EXISTS} -eq 0 ]; then
        echo "==> Insert admin user"

        PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "INSERT INTO users (email, password, token, updated, notifications, subscriptions, watched, preferences) VALUES ('admin', '"${ADMIN_PASSWORD_HASH}"', 'nE4k9MrOW1w4HeyfzsnzYIuqY6LkJU52Q0L989ngZmU=', NOW(), '{}', '{}', '{}', '{\"annotations\":false,\"annotations_subscribed\":false,\"autoplay\":false,\"automatic_instance_redirect\":false,\"captions\":[\"\",\"\",\"\"],\"comments\":[\"youtube\",\"\"],\"continue\":false,\"continue_autoplay\":true,\"dark_mode\":\"\",\"latest_only\":false,\"listen\":false,\"local\":false,\"watch_history\":true,\"vr_mode\":true,\"show_nick\":true,\"locale\":\"en-US\",\"region\":\"US\",\"max_results\":40,\"notifications_only\":false,\"player_style\":\"invidious\",\"quality\":\"hd720\",\"quality_dash\":\"auto\",\"default_home\":\"Popular\",\"feed_menu\":[\"Popular\",\"Trending\",\"Subscriptions\",\"Playlists\"],\"related_videos\":true,\"sort\":\"published\",\"speed\":1.0,\"thin_mode\":false,\"unseen_only\":false,\"video_loop\":false,\"extend_desc\":false,\"volume\":57,\"save_player_pos\":false}')"
    fi 
fi

# Invidious docs (https://docs.invidious.io/installation/#post-install-configuration) wants the users
# restart the app periodically.
echo "=> Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Invidious

