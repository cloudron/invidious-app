#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until, Key } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 30000;

    let browser, app;
    const admin_username='admin', admin_password="changeme";
    const MOVIE = 'dQw4w9WgXcQ';

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function login(username, password) {
        await browser.get(`https://${app.fqdn}/login`);

        await waitForElement(By.xpath('//input[@name="email"]'));
        await browser.findElement(By.xpath('//input[@name="email"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[@value="signin"]')).click();
        await browser.sleep(2000);
        await waitForElement(By.xpath('//span[@id="user_name" and text()="' + username  + '"]'));
    }

    async function getMainPage() {
        await browser.get('https://' + app.fqdn + '/feed/trending');
        await browser.sleep(2000);
        await waitForElement(By.xpath('//div[@class="thumbnail"]'));
    }

    async function watchMovie() {
        if (process.env.CI) {
            console.log('This test is skipped in CI');
            return;
        }
        await browser.get('https://' + app.fqdn);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@id="searchbox"]')).sendKeys(MOVIE);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@id="searchbox"]')).sendKeys(Key.RETURN);

        await browser.sleep(2000);

        await waitForElement(By.xpath('//a[@href="/watch?v=' + MOVIE + '"]'));
        await browser.findElement(By.xpath('//a[@href="/watch?v=' + MOVIE + '"]')).click();

        await waitForElement(By.xpath('//a[text()="Watch on YouTube"]'));

        await browser.findElement(By.xpath('//button[@title="Play Video"]')).click();
        await browser.sleep(12000);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login', login.bind(null, admin_username, admin_password));

    it('can get the main page', getMainPage);

    it('can watch movie', watchMovie);

    it('can restart app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
    });

    it('can get the main page', getMainPage);
    it('can watch movie', watchMovie);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can get the main page', getMainPage);
    it('can watch movie', watchMovie);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);
    it('can login', login.bind(null, admin_username, admin_password));
    it('can get the main page', getMainPage);
    it('can watch movie', watchMovie);


    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync(`cloudron install --appstore-id io.invidious.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login', login.bind(null, admin_username, admin_password));
    it('can get the main page', getMainPage);
    it('can watch movie', watchMovie);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('can get the main page', getMainPage);
    it('can watch movie', watchMovie);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});

