[0.1.0]
* Initial version for Invidious

[0.2.0]
* Fix for clipious

[0.3.0]
* Clean up code

[1.0.0]
* First stable release
* Invidious 2032-12-28

[1.1.0]
* Update Invidious to 2024.03.30

[1.2.0]
* Update Invidious to 08390acd0c

[1.3.0]
* Update Invidious to v2.20240427
* [Full changelog](https://github.com/iv-org/invidious/releases/tag/v2.20240427)
* Videos: Use android test suite client (#4650, thanks @SamantazFox)
* Trending: Un-nest category if this is the only one (#4600, thanks @ChunkyProgrammer)
* Comments: Add support for new format (#4576, thanks @ChunkyProgrammer)
* API: Add bitrate to formatStreams too (#4590, thanks @absidue)
* API: Add 'authorVerified' field on recommended videos (#4562, thanks @ChunkyProgrammer)
* Videos: Add support for new likes format (#4462, thanks @ChunkyProgrammer)
* Proxy: Handle non-200 HTTP codes on DASH manifests (#4429, thanks @absidue)

[1.3.1]
* Use tini to start the app

[1.3.2]
* Use supervisor to start/restart the app

[1.4.0]
* Update Invidious to v2.20240825
* [Full changelog](https://github.com/iv-org/invidious/releases/tag/v2.20240825)
* The search bar now has a button that you can click!
* Youtube URLs can be pasted directly in the search bar. Prepend search query with a backslash (\) to disable that feature (useful if you need to search for a video whose title contains some youtube URL).
* On the channel page the "streams" tab can be sorted by either: "newest", "oldest" or "popular"
* Lots of translations have been updated (thanks to our contributors on Weblate!)
* Videos embedded in local HTML files (e.g: a webpage saved from a blog) can now be played

[1.5.0]
* Implement `inv_sig_helper`

[1.5.1]
* Update Invidious to v2.20241110.0
* [Full changelog](https://github.com/iv-org/invidious/releases/tag/v2.20241110.0)
* Channels: Fix "Youtube API returned error 400" error preventing channel pages from loading
* Channels: Shorts can now be sorted by "newest", "oldest" and "popular"
* Preferences: Addition of the new "preload" option
* New interface languages available: Bulgarian, Welsh and Lombard
* Added "Filipino (auto-generated)" to the list of caption languages available
* Lots of new translations from Weblate

[1.5.2]
* Update sig helper to fix [crash](https://github.com/iv-org/inv_sig_helper/issues/36)

[1.5.3]
* Using latest sig helper to attempt fix the youtube js player changes

[1.5.4]
* Using latest sig helper to fix the youtube js player changes

[1.6.0]
* chore(deps): pin quay.io/invidious/inv-sig-helper docker tag to cd40f9b

