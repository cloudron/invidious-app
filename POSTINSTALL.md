This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin<br/>
**Password**: changeme<br/>

Please change the admin password immediately.

If it stops showing and playing videos you should delete `/app/data/.youtube-trusted-session-generator` file and restart the app.
