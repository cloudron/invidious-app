FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

# crystal language version - see https://docs.invidious.io/installation/#install-crystal
ARG CRYSTAL_VERSION=1.9
RUN echo 'deb http://download.opensuse.org/repositories/devel:/languages:/crystal/xUbuntu_23.04/ /' | tee /etc/apt/sources.list.d/devel:languages:crystal.list
RUN curl -fsSL https://download.opensuse.org/repositories/devel:languages:crystal/xUbuntu_23.04/Release.key | gpg --dearmor | tee /etc/apt/trusted.gpg.d/devel_languages_crystal.gpg > /dev/null
RUN apt-get update && \
    apt-get install -y --no-install-recommends crystal${CRYSTAL_VERSION} librsvg2-bin libsqlite3-dev libpcre3-dev libevent-dev liblzma-dev

RUN mkdir -p /app/build /app/pkg /app/code/config
WORKDIR /app/code

## install python3.12
## https://github.com/iv-org/youtube-trusted-session-generator/blob/master/Dockerfile#L1
RUN apt-get update && \
    add-apt-repository -y ppa:deadsnakes && \
    apt-get reinstall -y --no-install-recommends python3.12 python3.12-venv python3.12-dev python3-pip python3-setuptools virtualenv virtualenvwrapper && \
    curl -sS https://bootstrap.pypa.io/get-pip.py | python3.12

RUN update-alternatives --install /usr/bin/python python /usr/bin/python3.12 1 && \
    update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.12 1 && \
    mv /usr/local/bin/pip /usr/local/bin/pip-3.10 && \
    mv /usr/local/bin/pip3 /usr/local/bin/pip3.10 && \
    ln -s /usr/local/bin/pip3.12 /usr/local/bin/pip && \
    ln -s /usr/local/bin/pip3.12 /usr/local/bin/pip3
RUN pip install --upgrade pyasynchat supervisor==4.2.1

# install bcrypt to generate password hashes
RUN pip install --upgrade bcrypt setuptools

# https://github.com/iv-org/invidious/releases
# renovate: datasource=github-releases depName=iv-org/invidious versioning=loose extractVersion=^(?<version>.+)$
ARG INVIDIOUS_VERSION=v2.20241110.0

# see https://github.com/iv-org/invidious/blob/438467f69a20007cace4f300d03138b7d2d7e79a/src/invidious.cr#L78
RUN cd /app/build && \
    git clone https://github.com/iv-org/invidious && \
    cd invidious && \
    git reset --hard ${INVIDIOUS_VERSION} && \
    CURRENT_COMMIT=$(git rev-list HEAD --max-count=1 --abbrev-commit) && \
    CURRENT_VERSION=$(git log -1 --format=%ci | awk '{print $1}' | sed s/-/./g) && \
    ASSET_COMMIT=$(git rev-list HEAD --max-count=1 --abbrev-commit -- assets) && \
    sed -e "s/CURRENT_BRANCH  =.*$/CURRENT_BRANCH  = \"master\"/" \
        -e "s/CURRENT_COMMIT  =.*$/CURRENT_COMMIT  = \"${CURRENT_COMMIT}\"/" \
        -e "s/CURRENT_VERSION =.*$/CURRENT_VERSION = \"${CURRENT_VERSION}\"/" \
        -e "s/ASSET_COMMIT =.*$/ASSET_COMMIT = \"${ASSET_COMMIT}\"/" \
        -i /app/build/invidious/src/invidious.cr && \
    rm -rf /app/build/invidious/.git

RUN cd /app/build/invidious && \
    shards install --production && \
    make && \
    mv /app/build/invidious/{invidious,assets,locales,config} /app/code && \
    rm -rf /app/build

RUN ln -sf /app/data/config.yml /app/code/config/config.yml

# inv_sig_helper
COPY --from=quay.io/invidious/inv-sig-helper:latest@sha256:cd40f9bc62a809e6449b194c99c0cc908115585f014927e8e71ff803780ee0a4 /app/inv_sig_helper_rust /app/code/inv_sig_helper_rust

# Chromium, chromedriver
# https://github.com/iv-org/youtube-trusted-session-generator/blob/master/Dockerfile
RUN wget 'https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x869689FE09306074' -O '/etc/apt/trusted.gpg.d/phd-chromium.asc'
RUN echo "deb https://freeshell.de/phd/chromium/$(lsb_release -sc) /" | \
    tee /etc/apt/sources.list.d/phd-chromium.list

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        chromium \
        libatk1.0-0 \
        libatk-bridge2.0-0 \
        libxcomposite1 \
        libxdamage1 \
        xvfb \
        libfreetype6 \
        libfreetype6-dev \
        libharfbuzz0b \
        libharfbuzz-bin \
        fonts-freefont-ttf && \
    apt-get clean && \
    rm -rf /var/cache/apt /var/lib/apt/list/*

RUN CHROME_VERSION=$(curl -sS https://googlechromelabs.github.io/chrome-for-testing/last-known-good-versions.json | jq -r ".channels.Stable.version") && \
    curl -LfsSo /tmp/chromedriver.zip https://storage.googleapis.com/chrome-for-testing-public/${CHROME_VERSION}/linux64/chromedriver-linux64.zip && \
    unzip /tmp/chromedriver.zip && \
    mv ./chromedriver-linux64/chromedriver /usr/bin/chromedriver && \
    rm -rf ./chromedriver.zip ./chromedriver-linux64

RUN mkdir -p /app/code/youtube-trusted-session-generator && \
    curl -L https://github.com/iv-org/youtube-trusted-session-generator/archive/11f64ec105d8adda0b431ffab7e58597fa9ce1fd.tar.gz | \
    tar zxf - --strip-components 1 -C /app/code/youtube-trusted-session-generator

WORKDIR /app/code/youtube-trusted-session-generator
RUN pip install --no-cache-dir -r requirements.txt

# https://github.com/iv-org/youtube-trusted-session-generator/blob/master/Dockerfile#L22
#RUN sed -i 's/await self.sleep(0.5)/await self.sleep(2)/' /usr/local/lib/python3.10/dist-packages/nodriver/core/browser.py
RUN sed -i 's/await self.sleep(0.5)/await self.sleep(2)/' /usr/local/lib/python3.12/dist-packages/nodriver/core/browser.py


WORKDIR /app/code

ADD supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf

COPY start.sh /app/pkg/

CMD ["/app/pkg/start.sh"]
