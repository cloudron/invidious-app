## About

An open source alternative front-end to YouTube

## Features

* Lightweight
* No ads
* No tracking
* No JavaScript required
* Light/Dark themes
* Customizable homepage
* Subscriptions independent from Google
* Notifications for all subscribed channels
* Audio-only mode (with background play on mobile)
* Support for Reddit comments
* Available in many languages, thanks to our translators
* Import subscriptions from YouTube, NewPipe and Freetube
* Import watch history from YouTube and NewPipe
* Export subscriptions to NewPipe and Freetube
* Import/Export Invidious user data
